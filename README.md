# Toys Rent D-12

**Proyek Akhir Kelompok D-12 Mata Kuliah Basis Data, Fakultas Ilmu Komputer, Universitas Indonesia, Semester Genap 2018/2019**

Contributors:

| NPM        | Nama                                                            | Fitur            |
| :--------- | :-------------------------------------------------------------- | :--------------- |
| 1706043941 | [Bagus Pribadi](https://gitlab.com/)                            | 1 - 4            |
| 1706979190 | [Darin Amanda Zakiya](https://gitlab.com/darinamanda)           | 9 - 12           |
| 1706039742 | [Jeremy](https://gitlab.com/)                                   | 13 - 16          |
| 1706039603 | [Jose Devian Hibono](https://gitlab.com/)                       | 5 - 8            | 

**LINK** : https://toysrentd12.herokuapp.com/

## Developer Section
1. Clone Repo
> `https://gitlab.com/darinamanda/toysrent-d12.git`

2. Membuat Branch
> Fitur dikerjakan di branch masing-masing aja yaa untuk menghindari yang tidak diinginkan kalo langsung ngerjain dan push ke master. Nanti kalo udah tinggal merge request ke master ajaa.
> `git checkout -b [nama-branch]`

3. Push ke Branch
> `git push origin [nama-branch]`

4. Membuat App Baru
> Setiap mau membuat fitur, bikin app baru dulu. Masing-masing app adalah fitur yang dikerjakan. Supaya rapih dan mudah dimengerti yang lainnya, kalau bisa nama app adalah nama fitur. 
> `python manage.py startapp [namaapp]`
> Contoh: python manage.py startapp level
> Jangan lupa setiap membuat app baru, masukin nama appnya ke file settings.py project kita (nama folder project: toysrentd12) di bagian INSTALLED_APPS.
> Jangan lupa untuk edit file urls.py app, views.py app, dan juga urls.py project. 

5.  Collect Static
> Seluruh file static (css, js, img) ada di folder static. Setiap nambahin sesuatu atau mengutak atik folder static, jangan lupa untuk collect static.
> `python manage.py collectstatic`

6. Menjalankan di Lokal
> `python manage.py runserver`
