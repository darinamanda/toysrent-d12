from django import forms
from django.db import connection


class TambahPengiriman_Form(forms.Form):
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent')
    cursor.execute('select bp.id_pemesanan, bp.id_barang, b.nama_item from barang_pesanan bp, barang b '+'where bp.id_barang = b.id_barang')
    barangpesanan = cursor.fetchall()
    barangpesanan = list(barangpesanan)

    for i in range(len(barangpesanan)):
        barangpesanans= []
        barangku=[]
        barangpesanans.append(barangpesanan[i][1]+"-"+barangpesanan[i][2])
        barangpesanans.append(barangpesanan[i][0]+"-"+barangpesanan[i][1]+"-"+barangpesanan[i][2])
        barangku.append(barangpesanan[i][2])
        barangku.append(barangpesanan[i][2])
        barangpesanan[i] = tuple(barangpesanans)

    barang_pesanan = forms.MultipleChoiceField(label='Nama Item',required=True,choices=barangpesanan,widget=forms.CheckboxSelectMultiple(attrs={'name': 'barang','id': 'form' }))

    cursor.execute('select * from alamat')
    alamat = cursor.fetchall()
    alamat = list(alamat)

    for i in range(len(alamat)):
        address = []
        address.append(alamat[i][0] + " - " + alamat[i][1])
        address.append('Jl.' + alamat[i][2] + ", No. " + str(alamat[i][3]) + ", " + alamat[i][4] + \
                       " " + str(alamat[i][5]))
        alamat[i] = tuple(address)

    alamat_tujuan = forms.CharField(label='Alamat Tujuan',required=True,widget=forms.Select(choices=alamat,attrs={'name': 'alamat','id': 'form'}))

    tanggal_pengiriman = forms.DateField(label='Tanggal Pengiriman',required=True,widget=forms.SelectDateWidget(attrs={'id': 'form',}))

    metode = forms.CharField(label='Metode',required=True, widget=forms.TextInput(attrs={'id': 'form','placeholder': 'Teleportation'}))

    


