from django.shortcuts import render
from .forms import *
from django.db import connection
from django.http import HttpResponseRedirect
import string
import random

def tambahpengiriman(request):
	cursor = connection.cursor()
	cursor.execute('set search_path to toysrent')

	if 'jenis_akun' not in request.session.keys():
		return HttpResponseRedirect('/loginfirst')
	if request.method == 'POST':
		form = TambahPengiriman_Form(request.POST)
		if form.is_valid():
			objects = form.cleaned_data['barang_pesanan']
			alamat = form.cleaned_data['alamat_tujuan'].split(" - ")
			no_ktp_anggota = alamat[0]
			nama_alamat_anggota = alamat[1]
			tanggal_pengiriman = form.cleaned_data['tanggal_pengiriman']
			tanggal_pengiriman = tanggal_pengiriman.strftime('%Y') + "-" + tanggal_pengiriman.strftime('%m') + "-" + tanggal_pengiriman.strftime('%d')
			metode = form.cleaned_data['metode']
			ongkos = 9000

			asalasal = string.ascii_uppercase + string.digits
			intasal = string.digits

			for barang in objects:
				barang = barang.split(" - ")
				while True:
					no_resi = ''.join(random.choice(asalasal) for i in range(10))
					cursor.execute("select * from pengiriman where no_resi = '" + no_resi + "'")
					if len(cursor.fetchall()) == 0:
						break

			cursor.execute("insert into pengiriman values ('" + no_resi + "', '" + barang[0] + "', '" + metode + "', " + str(ongkos) + ", '" + tanggal_pengiriman + "', '" + no_ktp_anggota + "', '" + nama_alamat_anggota + "')")
			while True:
				no_urut = ''.join(random.choice(intasal) for i in range(10))
				cursor.execute("select * from barang_pesanan where no_urut = '" + no_urut + "'")
				if len(cursor.fetchall()) == 0:
					break
				cursor.execute(
                    "insert into barang_dikirim values ('" + no_resi + "', '" + no_urut + "', '" + barang[1] + "', '" + tanggal_pengiriman + "', '')")
			return HttpResponseRedirect('/daftarpengiriman')
		else:
			form = TambahPengiriman_Form()
	return render(request,'tambahpengiriman.html',{'forms':TambahPengiriman_Form})

def daftarpengiriman(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent')
    if 'jenis_akun' not in request.session.keys():
    	return HttpResponseRedirect('/loginfirst')
    cursor.execute('select * from pengiriman')

    shipping= cursor.fetchall()
    response['shippings'] = shipping

    for i in range(len(shipping)):
        cursor.execute(
            "select nama_item,id_pemesanan from barang b, barang_pesanan bp where bp.id_pemesanan = '" + shipping[i][1] + "' and bp.id_barang = b.id_barang")
        categories = cursor.fetchall()
        isishipping = list(response['shippings'][i])
       	isishipping.append(list(categories))

        response['shippings'][i] = isishipping

    return render(request,'daftarpengiriman.html',response)

def updatepengiriman(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent')
    if 'jenis_akun' not in request.session.keys():
    	return HttpResponseRedirect('/loginfirst')

    if request.method == 'POST':
        form = TambahPengiriman_Form(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('')
    else:
        form =  UpdatePengiriman_Form()
    return render(request, 'tambahpengiriman.html', {'forms': form})


def daftarreview(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent')
    if 'jenis_akun' not in request.session.keys():
    	return HttpResponseRedirect('/loginfirst')
    cursor.execute('select * from barang_dikirim')

    dikirim =cursor.fetchall()
    response['reviewankirim']=dikirim
    
    cursor.execute('select * from pengiriman')

    shipping= cursor.fetchall()


    for i in range(len(dikirim)):
    	cursor .execute("select nama_item,id_pemesanan from barang b, barang_pesanan bp where bp.id_pemesanan ='"+shipping[i][1]+"'and bp.id_barang=b.id_barang")
    	isi = cursor.fetchall()
    	isireview = list(response['reviewankirim'][i])
    	isireview.append(list(isi))
    	response['reviewankirim'][i] = isireview

    return render(request,'daftarreview.html',response)

def loginfirst(request):
	response={}

