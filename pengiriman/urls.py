from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('daftarpengiriman/', views.daftarpengiriman, name='daftarpengiriman'),
    path('tambahpengiriman/', views.tambahpengiriman, name='tambahpengiriman'),
    path('daftarreview/', views.daftarreview, name='daftarreview'),
    path('loginfirst/', views.loginfirst, name='loginfirst'),
]
