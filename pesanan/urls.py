from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('pemesanan/', views.pemesanan, name='pemesanan'),
    path('daftarpemesanan/', views.daftarpemesanan, name='daftarpemesanan'),
]