from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from .forms import Pemesanan_Form, UpdatePemesanan_Form

# Create your views here.
def pemesanan(request): # ini daftar pesanan
    # forms = Pemesanan_Form()
        response={}
        cursor = connection.cursor()
        cursor.execute('set search_path to toysrent')
        sql = 'select nama_lengkap from pengguna, anggota where pengguna.no_ktp = anggota.no_ktp'
        cursor.execute(sql)     
        daftar_nama= cursor.fetchall()
        cursor.execute('select nama_item from barang') 
        daftar_barang= cursor.fetchall()
        response= {'daftar_nama' : daftar_nama, 'daftar_barang' : daftar_barang}
        return render(request, 'pesanan.html',response)
        
    # return render(request,'pesanan.html',{'forms':forms})

def daftarpemesanan(request):
    # forms = UpdatePemesanan_Form()
    cursor = connection.cursor()
    cursor.execute("set search_path to toysrent")
    sql = "select bp.id_pemesanan, b.nama_item, p.harga_sewa, bp.status, bp.lama_sewa FROM barang_pesanan bp, barang b, pemesanan p where bp.id_barang = b.id_barang AND bp.id_pemesanan = p.id_pemesanan" 
    cursor.execute(sql)
    daftar = cursor.fetchall()
    cursor.execute("select nama_item from barang")
    pilihan_barang = cursor.fetchall()
    cursor.execute("select nama from status")
    pilihan_status = cursor.fetchall()
    response = {'daftar_pesanan' : daftar, 'pilihanbarang' : pilihan_barang, 'pilihanstatus' : pilihan_status}
    return render(request,'daftarpesanan.html',response)

    # return render(request,'daftarpesanan.html',{'forms':forms})

