from django import forms

class Pemesanan_Form(forms.Form):
    ANGGOTA1 = 'Darin'
    ANGGOTA2 ='Amanda'
    CATEGORY_CHOICES = (
        (ANGGOTA1, 'Darin'),
        (ANGGOTA2, 'Amanda'),
    )

    namalevel_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Nama Level',

    }

    lamasewa_attrs = {
        'class': 'form-control w-100',
        'type': 'text',
        'placeholder': 'Lama Sewa',
    }

    categoryname_attrs = {
        'class': 'btn dropdown-toggle',
        'type': 'text',
        'style':'width:100%;',
        'placeholder': 'Anggota',
    }

    nama_level = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=namalevel_attrs))
    lamasewa = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=lamasewa_attrs))
    category_name = forms.CharField(max_length = 70, label = '', required=True, widget = forms.Select(choices = CATEGORY_CHOICES, attrs=categoryname_attrs))

class UpdatePemesanan_Form(forms.Form):

    Status1 = 'Menunggu Konfirmasi Admin'
    Status2 ='Yuhu'
    CATEGORY_CHOICES = (
        (Status1, 'Menunggu Konfirmasi Admin'),
        (Status2, 'Yuhu'),
    )

    barangupdate_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Barang',

    }

    lamasewaupdate_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Lama Sewa'
    }

    statusupdate_attrs = {
        'class': 'form-control w-100',
        'type': 'text',
        'placeholder': 'Deskripsi',
    }

    barang_update = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=barangupdate_attrs))
    lamasewa_update = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=lamasewaupdate_attrs))
    status_update = forms.CharField(max_length = 70, label = '', required=True, widget = forms.Select(choices = CATEGORY_CHOICES, attrs=statusupdate_attrs))
