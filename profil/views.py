from django.shortcuts import render
from django.db import connection
from django.contrib import messages

# Create your views here.
def profil(request):
    if request.session['jenis_akun'] == 'pengguna':
        ktp = request.session['no_ktp']
        cursor = connection.cursor()
        cursor.execute('set search_path to toysrent')
        cursor.execute("SELECT poin from anggota where no_ktp='"+ktp+"'")
        poin = cursor.fetchall()
        for row in poin:
            poins = row[0]
        cursor.execute("SELECT level from anggota where no_ktp='"+ktp+"'")
        level = cursor.fetchall()
        for row in level:
            levels = row[0]
        request.session['poin'] = poins
        request.session['level'] = levels
        cursor.execute("SELECT nama from alamat where no_ktp_anggota='"+ktp+"'")
        nama = cursor.fetchall()
        for row in nama:
            namas = row[0]
        cursor.execute("SELECT jalan from alamat where no_ktp_anggota='"+ktp+"'")
        jalan = cursor.fetchall()
        for row in jalan:
            jalans = row[0]
        cursor.execute("SELECT nomor from alamat where no_ktp_anggota='"+ktp+"'")
        no = cursor.fetchall()
        for row in no:
            nos = row[0]
        cursor.execute("SELECT kota from alamat where no_ktp_anggota='"+ktp+"'")
        kota = cursor.fetchall()
        for row in kota:
            kotas = row[0]
        cursor.execute("SELECT kodepos from alamat where no_ktp_anggota='"+ktp+"'")
        kode = cursor.fetchall()
        for row in kode:
            kodes = row[0]
        request.session['addrs'] = namas+' '+jalans+' '+str(nos)+' '+kotas+' '+kodes
        return render(request,'profilPengguna.html')
    return render(request,'profilAdmin.html')