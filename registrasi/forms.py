from django import forms

class Admin_Form(forms.Form):
    noKTP_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'No KTP',
        'id': 'KTP',
    }

    namaLengkap_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Nama Lengkap',
        'id': 'name',
    }

    email_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Email',
        'id': 'email',
    }
    
    ttl_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Tanggal Lahir',
        'id': 'ttl',
    }
    
    noTelp_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Nomor Telepon/HP',
        'id': 'telp',
    }

    noKTP = forms.CharField(label = '', required=True, widget=forms.NumberInput(attrs=noKTP_attrs))
    namaLengkap = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=namaLengkap_attrs))
    email = forms.CharField(label = '', required=True, widget=forms.EmailInput(attrs=email_attrs))
    ttl = forms.CharField(label = '', required=False, widget=forms.DateInput(attrs=ttl_attrs))
    noTelp = forms.CharField(label = '', required=False, widget=forms.NumberInput(attrs=noTelp_attrs))
    
class Pengguna_Form(forms.Form):
    noKTP_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'No KTP',
        'id': 'KTP',
    }

    namaLengkap_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Nama Lengkap',
        'id': 'name',
    }

    email_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Email',
        'id': 'email',
    }
    
    ttl_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Tanggal Lahir',
        'id': 'ttl',
    }
    
    noTelp_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Nomor Telepon/HP',
        'id': 'telp',
    }
    
    addrsName_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Alamat: Nama',
        'id': 'aName',
    }
    
    addrsJalan_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Alamat: Jalan',
        'id': 'aJalan',
    }
    
    addrsNo_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Alamat: Nomor',
        'id': 'aNo',
    }
    
    addrsKota_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Alamat: Kota',
        'id': 'aKota',
    }
    
    addrsKode_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Alamat: Kode Pos',
        'id': 'aKode',
    }

    noKTP = forms.CharField(label = '', required=True, widget=forms.NumberInput(attrs=noKTP_attrs))
    namaLengkap = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=namaLengkap_attrs))
    email = forms.CharField(label = '', required=True, widget=forms.EmailInput(attrs=email_attrs))
    ttl = forms.CharField(label = '', required=False, widget=forms.DateInput(attrs=ttl_attrs))
    noTelp = forms.CharField(label = '', required=False, widget=forms.NumberInput(attrs=noTelp_attrs))
    addrsName = forms.CharField(label = '', required=False, widget=forms.TextInput(attrs=addrsName_attrs))
    addrsJalan = forms.CharField(label = '', required=False, widget=forms.TextInput(attrs=addrsJalan_attrs))
    addrsNo = forms.CharField(label = '', required=False, widget=forms.NumberInput(attrs=addrsNo_attrs))
    addrsKota = forms.CharField(label = '', required=False, widget=forms.TextInput(attrs=addrsKota_attrs))
    addrsKode = forms.CharField(label = '', required=False, widget=forms.TextInput(attrs=addrsKode_attrs))
    
class Login_Form(forms.Form):
    noKTP_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'No KTP',
        'id': 'KTP',
    }

    email_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Email',
        'id': 'email',
    }
    
    noKTP = forms.CharField(label = '', required=True, widget=forms.NumberInput(attrs=noKTP_attrs))
    email = forms.CharField(label = '', required=True, widget=forms.EmailInput(attrs=email_attrs))