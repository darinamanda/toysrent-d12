from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('adminReg/', views.adminReg, name='register'),
    path('penggunaReg/', views.penggunaReg, name='register'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('postAdmin/', views.postAdmin, name='postAdmin'),
    path('postPengguna/', views.postPengguna, name='postPengguna'),
    path('check/', views.check, name='check'),
]