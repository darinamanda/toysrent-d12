from django.shortcuts import render
from .forms import Admin_Form, Pengguna_Form, Login_Form
from django.http import HttpResponseRedirect, JsonResponse
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.core.validators import validate_email
from django.db import connection
from django.contrib import messages
from datetime import datetime

# Create your views here.
def adminReg(request):
    forms = Admin_Form()
    return render(request,'adminReg.html', {'forms':forms})
    
def penggunaReg(request):
    forms = Pengguna_Form()
    return render(request,'penggunaReg.html', {'forms':forms})
    
def login(request):
    forms = Login_Form()
    return render(request,'login.html', {'forms':forms})
    
def check(request):
    if request.method == 'POST':
        try:
            ktp = request.POST['no_ktp']
            email = request.POST['email']
            # request.session['no_ktp'] = user_id
            # request.session['email'] = user_email
            
            cursor = connection.cursor()
            cursor.execute('set search_path to toysrent')
            cursor.execute("SELECT * from pengguna where no_ktp='"+ktp+"'and email='"+email+"'" )
            res = cursor.fetchone()
            if(res):
                cursor.execute("SELECT nama_lengkap from pengguna where no_ktp='"+ktp+"'")
                nama = cursor.fetchall()
                for row in nama:
                    nama_lengkap = row[0]
                cursor.execute("SELECT tanggal_lahir from pengguna where no_ktp='"+ktp+"'")
                tanggal = cursor.fetchall()
                tanggal_lahir = str(tanggal[0])
                cursor.execute("SELECT no_telp from pengguna where no_ktp='"+ktp+"'")
                telp = cursor.fetchall()
                for row in telp:
                    no_telp = row[0]
                cursor.execute("SELECT * from admin where no_ktp='"+ktp+"'")
                resAdmin = cursor.fetchone()
                jenis = ''
                if(resAdmin):
                    jenis = 'admin'
                else:
                    jenis = 'pengguna'
                request.session['no_ktp'] = ktp
                request.session['nama_lengkap'] = nama_lengkap
                request.session['email'] = email
                request.session['tanggal_lahir'] = tanggal_lahir
                request.session['no_telp'] = no_telp
                request.session['jenis_akun'] = jenis
                return HttpResponseRedirect(reverse('homepage'))
            raise ValidationError("Akun tidak terdaftar!")
        except ValueError:
            return JsonResponse({"status": "1"})
    
def postAdmin(request):
    if request.method == 'POST':
        try:
            ktp = request.POST['no_ktp']
            nama = request.POST['nama_lengkap']
            email = request.POST['email']
            tanggal = request.POST['tanggal_lahir']
            telp = request.POST['no_telp']
            cursor = connection.cursor()
            cursor.execute('set search_path to toysrent')
            cursor.execute("SELECT * from pengguna where email='"+email+"'")
            hasil = cursor.fetchone()
            if(hasil):
                if len(hasil) > 0:
                    raise ValidationError("Email telah terdaftar!")
            cursor.execute("SELECT * from admin where no_ktp='"+ktp+"'")
            hasil = cursor.fetchone()
            if(hasil):
                if len(hasil) > 0:
                    raise ValidationError("Nomor ktp telah terdaftar!")
            cursor.execute("INSERT INTO pengguna (no_ktp,nama_lengkap,email,tanggal_lahir,no_telp) values ('"+ktp+"','"+nama+"','"+email+"','"+tanggal+"','"+telp+"')")
            cursor.execute("INSERT INTO admin (no_ktp) values ('"+ktp+"')")
            return HttpResponseRedirect(reverse('login'))
        except ValueError:
            return JsonResponse({"status": "1"})

def postPengguna(request):
    if request.method == 'POST':
        try:
            ktp = request.POST['no_ktp']
            nama = request.POST['nama_lengkap']
            email = request.POST['email']
            tanggal = request.POST['tanggal_lahir']
            telp = request.POST['no_telp']
            aName = request.POST['aName']
            aJalan = request.POST['aJalan']
            aNo = request.POST['aNo']
            aKota = request.POST['aKota']
            aKode = request.POST['aKode']
            level = 'Bronze'
            poin = '0'
            cursor = connection.cursor()
            cursor.execute('set search_path to toysrent')
            cursor.execute("SELECT * from pengguna where email='"+email+"'")
            hasil = cursor.fetchone()
            if(hasil):
                if len(hasil) > 0:
                    raise ValidationError("Email telah terdaftar!")
            cursor.execute("SELECT * from anggota where no_ktp='"+ktp+"'")
            hasil = cursor.fetchone()
            if(hasil):
                if len(hasil) > 0:
                    raise ValidationError("Nomor ktp telah terdaftar!")
            cursor.execute("INSERT INTO pengguna (no_ktp,nama_lengkap,email,tanggal_lahir,no_telp) values ('"+ktp+"','"+nama+"','"+email+"','"+tanggal+"','"+telp+"')")
            cursor.execute("INSERT INTO anggota (no_ktp,poin,level) values ('"+ktp+"',"+poin+",'"+level+"')")
            cursor.execute("INSERT INTO alamat (no_ktp_anggota,nama,jalan,nomor,kota,kodepos) values ('"+ktp+"','"+aNama+"','"+aJalan+"',"+aNo+",'"+aKota+"','"+aKode+"')")
            return HttpResponseRedirect(reverse('login'))
        except ValueError:
            return JsonResponse({"status": "1"})
    
def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('homepage'))