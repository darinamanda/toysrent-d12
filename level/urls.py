from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('tambahlevel/', views.tambahlevel, name='tambahlevel'),
    path('daftarlevel/', views.daftarlevel, name='daftarlevel'),
    path('tambahlevel-success/', views.create_level_post, name='postlevel'),
]