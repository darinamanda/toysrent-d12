from django import forms

class TambahLevel_Form(forms.Form):
    # AKADEMIS = 'Akademis'
    # KEPANITIAAN ='Kepanitiaan dan Organisasi'
    # KELUARGA = 'Keluarga'
    # OLAHRAGA = 'Olahraga'
    # MAIN = 'Main'
    # OTHER = 'Other'
    # CATEGORY_CHOICES = (
    #     (AKADEMIS, 'Akademis'),
    #     (KEPANITIAAN, 'Kepanitiaan dan Organisasi'),
    #     (KELUARGA, 'Keluarga'),
    #     (OLAHRAGA, 'Olahraga'),
    #     (MAIN, 'Main'),
    #     (OTHER, 'Other'),
    # )
    namalevel_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Nama Level',

    }

    minimumpoin_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Minimum Poin'
    }

    deskripsi_attrs = {
        'class': 'form-control w-100',
        'type': 'text',
        'placeholder': 'Deskripsi',
    }

    nama_level = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=namalevel_attrs))
    minimum_poin = forms.CharField(label = '', required=True, widget=forms.NumberInput(attrs=minimumpoin_attrs))
    deskripsi = forms.CharField(label = '', required=False, widget=forms.TextInput(attrs=deskripsi_attrs))
    # category_name = forms.CharField(max_length = 70, label = '', required=True, widget = forms.Select(choices = CATEGORY_CHOICES))


class UpdateLevel_Form(forms.Form):

    namalevelupdate_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Nama Level',

    }

    minimumpoinupdate_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Minimum Poin'
    }

    deskripsiupdate_attrs = {
        'class': 'form-control w-100',
        'type': 'text',
        'placeholder': 'Deskripsi',
    }

    nama_level_update = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=namalevelupdate_attrs))
    minimum_poin_update = forms.CharField(label = '', required=True, widget=forms.NumberInput(attrs=minimumpoinupdate_attrs))
    deskripsi_update = forms.CharField(label = '', required=False, widget=forms.TextInput(attrs=deskripsiupdate_attrs))
