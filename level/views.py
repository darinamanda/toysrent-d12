from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.db import connection
from .forms import TambahLevel_Form, UpdateLevel_Form

# Create your views here.
def tambahlevel(request):
    # forms = TambahLevel_Form()
    return render(request,'tambahlevel.html', {})

def daftarlevel(request):
    # forms = UpdateLevel_Form()
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent')
    sql = "select nama_level, deskripsi, minimum_poin from level_keanggotaan"
    cursor.execute(sql)
    pilihan_level = cursor.fetchall()
    response = {'pilihan_level' : pilihan_level}
    return render(request,'daftarlevel.html',response)

def create_level_post(request):
    if(request.method=="POST"):
        nama_level = request.POST['nama_level']
        min_poin = request.POST['min_poin']
        deskripsi = request.POST['deskripsi']

        cursor = connection.cursor()
        cursor.execute('set search_path to toysrent')
        cursor.execute("select * from level_keanggotaan where nama_level='"+nama_level+"'")
        res = cursor.fetchone()
        if(res):
            if len(res) > 0:
                return HttpResponseRedirect('/tambahlevel/')
        sql = "insert into level_keanggotaan(nama_level, minimum_poin, deskripsi) values("+"'"+nama_level+"'," + str(min_poin)+",'"+deskripsi+"');"
        cursor.execute(sql)

        return HttpResponseRedirect('/daftarlevel/')

    else:
        return HttpResponseRedirect('/tambahlevel/')

    
    # return render(request,'daftarlevel.html', {'forms':forms})

