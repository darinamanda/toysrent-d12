from django import forms

class TambahBarang_Form(forms.Form):
    # AKADEMIS = 'Akademis'
    # KEPANITIAAN ='Kepanitiaan dan Organisasi'
    # KELUARGA = 'Keluarga'
    # OLAHRAGA = 'Olahraga'
    # MAIN = 'Main'
    # OTHER = 'Other'
    # CATEGORY_CHOICES = (
    #     (AKADEMIS, 'Akademis'),
    #     (KEPANITIAAN, 'Kepanitiaan dan Organisasi'),
    #     (KELUARGA, 'Keluarga'),
    #     (OLAHRAGA, 'Olahraga'),
    #     (MAIN, 'Main'),
    #     (OTHER, 'Other'),
    # )
    url_foto_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'URL Foto',
    }

    nama_item_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Nama Item'
    }

    warna_item_attrs = {
        'class': 'form-control w-100',
        'type': 'text',
        'placeholder': 'Warna',
    }

    kondisi_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Kondisi'
    }

    lama_penggunaan_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Lama Penggunaan (hari)'
    }

    id_barang_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'ID Barang'
    }

    pemilik_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Pemilik'
    }

    id_barang = forms.IntegerField(label = '', required=False, min_value=1, widget=forms.NumberInput(attrs=id_barang_attrs))
    nama_item = forms.CharField(label = '', required=False, widget=forms.TextInput(attrs=nama_item_attrs))
    warna_item = forms.CharField(label = '', required=False, widget=forms.TextInput(attrs=warna_item_attrs))
    url_foto = forms.CharField(label = '', required=False, widget=forms.TextInput(attrs=url_foto_attrs))
    kondisi = forms.CharField(label = '', required=False, widget=forms.TextInput(attrs=kondisi_attrs))
    lama_penggunaan = forms.IntegerField(label = '', required=False, min_value=1, widget=forms.TextInput(attrs=lama_penggunaan_attrs))
    pemilik = forms.CharField(label='', required=False, widget=forms.TextInput(attrs=pemilik_attrs))
    # category_name = forms.CharField(max_length = 70, label = '', required=True, widget = forms.Select(choices = CATEGORY_CHOICES))


class UpdateBarang_Form(forms.Form):

    url_fotoupdate_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Alamat URL Foto',
    }

    nama_itemupdate_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Nama Item'
    }

    warna_itemupdate_attrs = {
        'class': 'form-control w-100',
        'type': 'text',
        'placeholder': 'Warna',
    }

    kondisiupdate_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Kondisi'
    }

    lama_penggunaanupdate_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Lama Penggunaan (bulan)'
    }

    id_barang_update_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'ID Barang'
    }

    pemilik_update_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Pemilik'
    }

    id_barang_update = forms.IntegerField(label = '', required=True, min_value=1, widget=forms.NumberInput(attrs=id_barang_update_attrs))
    nama_item_update = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=nama_itemupdate_attrs))
    warna_item_update = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=warna_itemupdate_attrs))
    url_foto_update = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=url_fotoupdate_attrs))
    kondisi_update = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=kondisiupdate_attrs))
    lama_penggunaan_update = forms.IntegerField(label = '', required=True, min_value=1, widget=forms.TextInput(attrs=lama_penggunaanupdate_attrs))
    pemilik_update = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=pemilik_update_attrs))
