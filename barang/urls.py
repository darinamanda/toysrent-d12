from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('daftarbarang/', views.daftarbarang, name='daftarbarang'),
    path('tambahbarang/', views.tambahbarang, name='tambahbarang'),
]
