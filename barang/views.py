from django.shortcuts import render
from django.db import connection
from .forms import TambahBarang_Form, UpdateBarang_Form

# Create your views here.
def tambahbarang(request):
    forms = TambahBarang_Form()
    return render(request,'tambahbarang.html', {'forms':forms})

def daftarbarang(request):
    forms = UpdateBarang_Form()
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent')
    sql = "select id_barang, nama_item, warna, url_foto, kondisi from barang"
    cursor.execute(sql)
    pilihan_barang = cursor.fetchall()
    response = {'pilihan_barang' : pilihan_barang}
    return render(request,'daftarbarang.html', response)

def detailbarang(request, id_barang = 1):
    cursor = connection.cursor()
    sql = "SELECT id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan, nama_lengkap FROM toys_rent.barang JOIN toys_rent.pengguna ON toys_rent.barang.no_ktp_penyewa = toys_rent.pengguna.no_ktp WHERE id_barang = CAST(" + str(id_barang) + " AS VARCHAR(10));"
    cursor.execute(sql)
    detail_info = cursor.fetchall()[0]
    response['id_barang'] = detail_info[0]
    response['nama_item'] = detail_info[1]
    response['warna'] = detail_info[2]
    response['url_foto'] = detail_info[3]
    response['kondisi'] = detail_info[4]
    response['lama_penggunaan'] = detail_info[5]
    response['nama_lengkap'] = detail_info[6]
    return render(request, 'daftarbarang.html#detail1', response)
