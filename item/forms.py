from django import forms

class Item_Form(forms.Form):
    nama_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Nama Item',
        'id': 'name',
    }

    deskripsi_attrs = {
        'class': 'form-control w-100',
        'type': 'text',
        'placeholder': 'Deskripsi',
        'id': 'desc',
    }

    rentang1_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Usia Dari',
        'id': 'age1',
    }
    
    rentang2_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Usia Sampai',
        'id': 'age2',
    }
    
    bahan_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Bahan',
        'id': 'bahan',
    }
    
    kategori_attrs = {
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Kategori',
        'id': 'kategori',
    }

    nama = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=nama_attrs))
    deskripsi = forms.CharField(label = '', required=False, widget=forms.TextInput(attrs=deskripsi_attrs))
    rentang1 = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=rentang1_attrs))
    rentang2 = forms.CharField(label = '', required=True, widget=forms.TextInput(attrs=rentang2_attrs))
    bahan = forms.CharField(label = '', required=False, widget=forms.TextInput(attrs=bahan_attrs))
    kategori = forms.ChoiceField(label = '', required=False, choices=[('Motorik Kasar', 'Motorik Kasar'), ('Motorik Halus', 'Motorik Halus'), ('Musik Mainan', 'Musik Mainan'), ('Car Seat', 'Car Seat'), ('Baby Walker', 'Baby Walker')])