from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('tambahitem/', views.tambahitem, name='tambahitem'),
    path('daftaritem/', views.daftaritem, name='daftaritem'),
    path('postItem/', views.postItem, name='postItem'),
]