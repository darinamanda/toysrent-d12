from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .forms import Item_Form
from django.db import connection
from django.contrib import messages

# Create your views here.
def tambahitem(request):
    forms = Item_Form()
    return render(request,'tambahitem.html', {'forms':forms})

def daftaritem(request):
    response={}
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent')
    cursor.execute("SELECT K.nama_item, K.nama_kategori, I.deskripsi, I.usia_dari, I.usia_sampai, I.bahan FROM kategori_item K, item I WHERE I.nama = K.nama_item")
    hasil1 = cursor.fetchall()
    response['item'] = hasil1
    cursor.execute("select nama from kategori")
    hasil2 = cursor.fetchall()
    response['kategori'] = hasil2
    return render(request,'daftaritem.html', response)
    
def postItem(request):
    try:
        name = request.POST['name']
        desc = request.POST['desc']
        age1 = request.POST['age1']
        age2 = request.POST['age2']
        bahan = request.POST['bahan']
        kategori = request.POST['kategori']
        cursor = connection.cursor()
        cursor.execute('set search_path to toysrent')
        cursor.execute("SELECT * from item where nama='"+name+"'")
        hasil = cursor.fetchone()
        if(hasil):
            if len(hasil) > 0:
                raise ValidationError("Item telah terdaftar!")
        cursor.execute("INSERT INTO item (nama,deskripsi,usia_dari,usia_sampai,bahan) values ('"+name+"','"+desc+"',"+age1+","+age2+",'"+bahan+"')")
        cursor.execute("INSERT INTO kategori_item (nama_item,kategori_item) values ('"+name+"','"+kategori+"')")
        raise ValidationError("Coba-coba")
    except:
        return JsonResponse({"status": "1"})